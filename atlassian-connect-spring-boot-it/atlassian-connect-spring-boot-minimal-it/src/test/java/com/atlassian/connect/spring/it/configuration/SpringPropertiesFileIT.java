package com.atlassian.connect.spring.it.configuration;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "management.context-path: /override")
public class SpringPropertiesFileIT extends BaseApplicationIT {
    
    @Value("${management.context-path}")
    private String managementContextPath;
    
    @Test
    public void shouldOverrideDefaultConfigValue() {
        assertEquals(managementContextPath, "/override");
    }
}
